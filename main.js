import Vue from 'vue'
import App from './App'
import titleBar from '@/components/titleBar.vue'

Vue.config.productionTip = false

App.mpType = 'app'

// Vue.prototype.tip = function(title, icon) {
// 	uni.showToast({
// 		title: title,
// 		image: '../../static/' + icon + '.png',
// 		duration: 2000
// 	})
// }

Vue.component('titleBar', titleBar)

const app = new Vue({
    ...App
})
app.$mount()
